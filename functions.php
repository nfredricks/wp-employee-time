<?php
function start_tracking(){
  
  $gofs = get_option( 'gmt_offset' ); // get WordPress offset in hours
  $tz = date_default_timezone_get(); // get current PHP timezone
  date_default_timezone_set('Etc/GMT'.(($gofs < 0)?'+':'').-$gofs); // set the PHP timezone to match WordPress
  global $wpdb;
  $table_name = $wpdb->prefix ."employee_time";

  $date = date('Y-m-d');
  $cur_time = date('H:i:s');

  $track_check = track_check();

  if($track_check['track']){
    if(!is_tracking($table_name, $wpdb)){
      $wpdb->insert( $table_name, array( 'user' => $track_check['id'], 'day' => $date, 'online' => 1, 'start' => $cur_time, 'end' => $cur_time) );
    }
  }

  date_default_timezone_set($tz); // set the PHP timezone back the way it was

}

function end_tracking($wpdb){
  $gofs = get_option( 'gmt_offset' ); // get WordPress offset in hours
  $tz = date_default_timezone_get(); // get current PHP timezone
  date_default_timezone_set('Etc/GMT'.(($gofs < 0)?'+':'').-$gofs); // set the PHP timezone to match WordPress
  $table_name = $wpdb->prefix ."employee_time";

  $date = date('Y-m-d');
  $cur_time = date('H:i:s');

  $track_check = track_check();

  if($track_check['track']){
    //update end time to right now
    $update = array('end' => $cur_time);
    $where = array('user' => $track_check['id'], 'day' => $date, 'online' => 1 );
    $wpdb->update($table_name, $update, $where);

    //set user to offline
    remove_online($track_check['id'], $date, $table_name, $wpdb);
  }

  date_default_timezone_set($tz); // set the PHP timezone back the way it was
}

function remove_online($id, $date, $table_name, $wpdb){
  $update = array('online' => 0);
  $where = array('user' => $id, 'day' => $date, 'online'=> 1 );
  $wpdb->update($table_name, $update, $where);
}

function track_check(){
  $user = wp_get_current_user();
  $roles = $user->roles;
  $id = $user->id;
  $types = get_option('tracking_types');
  $types = explode(',',$types);
  foreach($roles as $role){
    foreach($types as $type){
      if($role == trim($type)){
        return array('track' => true, 'id' => $id, 'roles' => $roles);
      }
    }
  }
  return array('track' => false, 'id' => $id, 'roles' => $roles);
}
function check_admin(){
  $user = wp_get_current_user();
  $roles = $user->roles;
  $id = $user->id;
  $type = 'administrator';
  foreach($roles as $role){
      if($role == trim($type)){
        return array('admin' => true, 'id' => $id, 'roles' => $roles);
      }
  }
  return array('admin' => false, 'id' => $id, 'roles' => $roles);
}

function is_tracking($table_name, $wpdb, $id){
  if($id == ""){
    $user = wp_get_current_user();
    $id = $user->id;
  }
  $result = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE user = %s AND online = 1 ", $id));
  if($result[0] == NULL){
    return false;
  }
  return true;
}

function time_since_last_up($cur_time, $table_name, $today, $wpdb){

  $user = wp_get_current_user();
  $id = $user->id;

  $result = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE user = %s AND day = %s AND online = 1 ", $id, $today));
  $end = $result[0]->end;

  list($year, $month, $day) = explode("-", $today);
  list($hour, $min, $sec) = explode(":",$end);
  $end_utime = mktime($hour, $min, $sec, $month, $day, $year);

  list($hour, $min, $sec) = explode(":",$cur_time);
  $cur_utime = mktime($hour, $min, $sec, $month, $day, $year);
  return $cur_utime - $end_utime;
}

function utime_from_datetime($date, $time){
  list($year, $month, $day) = explode("-", $date);
  list($hour, $min, $sec) = explode(":",$time);
  return(mktime($hour, $min, $sec, $month, $day, $year));
}



function get_week($offset) {
  $gofs = get_option( 'gmt_offset' ); // get WordPress offset in hours
  $tz = date_default_timezone_get(); // get current PHP timezone
  date_default_timezone_set('Etc/GMT'.(($gofs < 0)?'+':'').-$gofs); // set the PHP timezone to match WordPress
    $date = date("Y-m-d");

    if($offset != ""){
      $date = date("Y-m-d", strtotime($offset." week"));
    }

    list($year, $month, $day) = explode("-", $date);

    // Get the weekday of the given date
    $wkday = date('l',mktime('0','0','0',$month,$day,$year));
//echo $wkday;
    switch($wkday) {
        case 'Monday': $numDaysToMon = 0; break;
        case 'Tuesday': $numDaysToMon = 1; break;
        case 'Wednesday': $numDaysToMon = 2; break;
        case 'Thursday': $numDaysToMon = 3; break;
        case 'Friday': $numDaysToMon = 4; break;
        case 'Saturday': $numDaysToMon = 5; break;
        case 'Sunday': $numDaysToMon = 6; break;   
    }


    // Timestamp of the monday for that week
    $monday = mktime('0','0','0', $month, $day-$numDaysToMon, $year);

    $seconds_in_a_day = 86400;

    // Get date for opt days from Monday (inclusive)
    for($i=0; $i<7; $i++)
    {
        $dates[$i] = date('Y-m-d',$monday+($seconds_in_a_day*$i));
    }
      date_default_timezone_set($tz); // set the PHP timezone back the way it was
    return $dates;
}

function time_for_day($date, $id, $wpdb, $table_name){
  $results = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE user = %s AND day = %s", $id, $date));
  $sum = 0;
  foreach($results as $result){
    $start_utime = utime_from_datetime($date, $result->start);
    $end_utime = utime_from_datetime($date, $result->end);
    $sum += $end_utime-$start_utime;
  }
  $hours = $sum/3600;
  $round_up = number_format(round($hours*4)/4, 2);
  $round_down = number_format(floor($hours*4)/4, 2);
  $up_diff = abs($round_up-$hours);
  $dn_diff = abs($round_down-$hours);
  if($up_diff <= $dn_diff){
    $num = $round_up;
  }
  else{
    $num = $round_down;
  }

  return array('raw' => round($hours, 2), 'rounded' => $num);
}

function convert_number_to_words($number) {
    
    $hyphen      = '-';
    $conjunction = ' and ';
    $separator   = ', ';
    $negative    = 'negative ';
    $decimal     = ' point ';
    $dictionary  = array(
        0                   => 'zero',
        1                   => 'one',
        2                   => 'two',
        3                   => 'three',
        4                   => 'four',
        5                   => 'five',
        6                   => 'six',
        7                   => 'seven',
        8                   => 'eight',
        9                   => 'nine',
        10                  => 'ten',
        11                  => 'eleven',
        12                  => 'twelve',
        13                  => 'thirteen',
        14                  => 'fourteen',
        15                  => 'fifteen',
        16                  => 'sixteen',
        17                  => 'seventeen',
        18                  => 'eighteen',
        19                  => 'nineteen',
        20                  => 'twenty',
        30                  => 'thirty',
        40                  => 'fourty',
        50                  => 'fifty',
        60                  => 'sixty',
        70                  => 'seventy',
        80                  => 'eighty',
        90                  => 'ninety',
        100                 => 'hundred',
        1000                => 'thousand',
        1000000             => 'million',
        1000000000          => 'billion',
        1000000000000       => 'trillion',
        1000000000000000    => 'quadrillion',
        1000000000000000000 => 'quintillion'
    );
    
    if (!is_numeric($number)) {
        return false;
    }
    
    if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
        // overflow
        trigger_error(
            'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
            E_USER_WARNING
        );
        return false;
    }

    if ($number < 0) {
        return $negative . convert_number_to_words(abs($number));
    }
    
    $string = $fraction = null;
    
    if (strpos($number, '.') !== false) {
        list($number, $fraction) = explode('.', $number);
    }
    
    switch (true) {
        case $number < 21:
            $string = $dictionary[$number];
            break;
        case $number < 100:
            $tens   = ((int) ($number / 10)) * 10;
            $units  = $number % 10;
            $string = $dictionary[$tens];
            if ($units) {
                $string .= $hyphen . $dictionary[$units];
            }
            break;
        case $number < 1000:
            $hundreds  = $number / 100;
            $remainder = $number % 100;
            $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
            if ($remainder) {
                $string .= $conjunction . convert_number_to_words($remainder);
            }
            break;
        default:
            $baseUnit = pow(1000, floor(log($number, 1000)));
            $numBaseUnits = (int) ($number / $baseUnit);
            $remainder = $number % $baseUnit;
            $string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
            if ($remainder) {
                $string .= $remainder < 100 ? $conjunction : $separator;
                $string .= convert_number_to_words($remainder);
            }
            break;
    }
    
    if (null !== $fraction && is_numeric($fraction)) {
        $string .= $decimal;
        $words = array();
        foreach (str_split((string) $fraction) as $number) {
            $words[] = $dictionary[$number];
        }
        $string .= implode(' ', $words);
    }
    
    return $string;
}

function update_timesheet_file() {
    global $wpdb;

    $opt_name = 'tracking_types';
    $opt_val = get_option( $opt_name );
    $types = explode(',', $opt_val);

    $table_name = $wpdb->prefix . "usermeta"; 
    $result = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE meta_key = 'wp_capabilities' "));
    $user_id_array = array();
    foreach($result as $user){
        if(is_trackable($types, $user)){
            array_push($user_id_array, $user->user_id);
        }
    }

    $employ_table_name = $wpdb->prefix ."employee_time";
    $objPHPExcel = new PHPExcel();

    $objPHPExcel->getProperties()->setCreator("Timesheet Bot")
                               ->setLastModifiedBy("Timesheet Bot")
                               ->setTitle("Employee Timesheets")
                               ->setSubject("Employee Timesheets")
                               ->setDescription("Employee Timesheets");

    for ($i = 0; $i < count($user_id_array); $i++) {
        $result = $wpdb->get_results($wpdb->prepare("SELECT * FROM $employ_table_name WHERE user = %s ", $user_id_array[$i] ));
        $user_handle = get_user_by('id', $user_id_array[$i]);

        $objPHPExcel->createSheet();
        $objPHPExcel->setActiveSheetIndex($i);
        $objPHPExcel->getActiveSheet()->setTitle($user_handle->user_login);
        foreach ($result as $user) {
            //write contents

        }
        
        

    }
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save(str_replace('functions.php', 'master_timesheet.xlsx', __FILE__));


}

function is_trackable($types, $user){

$cap = key(unserialize($user->meta_value));  
foreach($types as $type){
    if($cap == trim($type)){
        return true;
    }
}
return false;

}

require_once('PHPExcel.php');
require_once('PHPExcel/IOFactory.php');


?>
