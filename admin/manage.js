jQuery(document).ready(function($){
    $('select#employee_selector').on('change',function(){
        $('select#employee_selector option:selected').each(function(){
            //do some ajax to get timesheet of $(this).attr('value')
            var data = {
                action: 'timesheet_grab',
                id: $(this).attr('value')
            };
            $.post(ajaxurl, data, function(response) {
                $('div#timesheet_space').html(response);

            });
        });
        
    });
});