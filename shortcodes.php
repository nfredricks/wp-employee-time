<?php
function timesheet_table($atts){
	global $wpdb;
	$table_name = $wpdb->prefix."employee_time";
    extract( shortcode_atts( array('offset' => '0', 'id' => get_current_user_id()), $atts));
    
    $total_hours = 0;
    $raw_hours = 0;
    if($offset == '0'){
    	$title = "Current Week";
    }
    elseif($offset == '1'){
    	$title = ucfirst(convert_number_to_words($offset))." Week Ago";
    }
    else{
    	$title = ucfirst(convert_number_to_words($offset))." Weeks Ago";
    }
    $offset = "-".$offset;
    $week = get_week($offset);
	?>
	<table class="time_sheet_table">
        <thead>
          <tr>
            <th colspan=2><?php echo $title; ?></th>
          </tr>
          <tr>
            <th>Day</th>
            <th>Hours</th>
          </tr>
        </thead>
        <tbody>
      <?php

       for($i=0; $i<7; $i++){
          echo "<tr>";
          list($year, $month, $day) = explode("-", $week[$i]);
          $day = date('l, M d',mktime('0','0','0',$month,$day,$year));
          $hours = time_for_day($week[$i], $id, $wpdb, $table_name);
          echo "<td>".$day."</td>";
          echo "<td>".$hours['rounded']." (actual: ".$hours['raw'].")</td>";
          echo "</tr>";
          $total_hours += $hours['rounded'];
          $raw_hours += $hours['raw'];
       }
       echo "<tr><td colspan=2><h4>Total Hours: ".$total_hours." (actual: ".$raw_hours.")</h4></td></tr>";
      ?>
      </tbody>
    </table>
    <?php
  }



  add_shortcode( 'show-timesheet', 'timesheet_table' );
?>
