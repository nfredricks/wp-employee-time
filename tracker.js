jQuery(document).ready(function($){
	(function track(ajaxurl){
		var data = {
			action: 'employ_track'
		};
		$.post(ajaxurl, data, function(response) {
			console.log('Got this from the server: ' + response);
            console.log(typeof(response));
            var users = $.parseJSON(response);
            if(response != "" && response != "[]"){
                var widget = $('#online_widget-2').find('div.info ul');
                var html ="";
                for(var i=0; i<users.length; i++){
                    html += users[i];
                }
                widget.html(html);
            }
		});
		setTimeout(function(){track(ajaxurl)}, 10000);
	})(track.ajaxurl, 'employ_track');
    
    $('.ab-clocker').on('click',function(e){
        e.preventDefault();
        label = $(this).find('.ab-label');
        
        old = label.html();
        label.html(label.attr('data-next'));
        label.attr('data-next', old);
        
        
        
        var data = {
            action: 'ab_start_track',
            ab_clocker: '1'
        }
        $.post(track.ajaxurl, data, function(response) {
            console.log('Started or Stopped?: '+ response);
        });

    });
    
});

