<?php
//widget

class online_widget extends WP_Widget {

// Constructor //

  function online_widget() {
    $widget_ops = array( 'classname' => 'online_widget', 'description' => 'Displays online employees' ); // Widget Settings
    $control_ops = array( 'id_base' => 'online_widget' ); // Widget Control Settings
    $this->WP_Widget( 'online_widget', 'Online Employees', $widget_ops, $control_ops ); // Create the widget
  }
  // Extract Args //
  function widget($args, $instance) {
    global $wpdb;
    extract( $args );
    $title     = apply_filters('widget_title', $instance['title']); // the widget title
    $users_to_show = $instance['num_users']; // grab how many users to show


    // Before widget //

      echo $before_widget;

  // Title of widget //

      if ( $title ) { echo $before_title . $title . $after_title; }

  // Widget output //
      $table_name = $wpdb->prefix . "employee_time";
      $results = $wpdb->get_results($wpdb->prepare("SELECT user FROM $table_name WHERE online = 1 LIMIT $users_to_show "));
      
      echo "<div class=info>";
      echo "<ul>";
      foreach($results as $result){
        $id = $result->user;
        //poll wp_users for $user

        $user_info = get_userdata($id);
        echo "<li>".$user_info->display_name."</li>";
      }
      echo "</ul>";
      echo "</div>";


  // After widget //

      echo $after_widget;
    }

      // Update Settings //

    function update($new_instance, $old_instance) {
      $instance['title'] = strip_tags($new_instance['title']);
      $instance['num_users'] = strip_tags($new_instance['num_users']);
      return $instance;
    }

    // Widget Control Panel //

    function form($instance) {

    $defaults = array( 'title' => 'Online Employees', 'num_users' => '8');
    $instance = wp_parse_args( (array) $instance, $defaults ); ?>

    <p>
      <label for="<?php echo $this->get_field_id('title'); ?>">Title:</label>
      <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>'" type="text" value="<?php echo $instance['title']; ?>" />
    </p>
     <p>
      <label for="<?php echo $this->get_field_id('link_titles'); ?>">Number of Users to Show</label>
      <input class="widefat" id="<?php echo $this->get_field_id('num_users'); ?>" name="<?php echo $this->get_field_name('num_users'); ?>" value ="<?php echo $instance['num_users']; ?>" >
    </p>
 <?php }

}


add_action('widgets_init', create_function('', 'return register_widget("online_widget");'));
?>
