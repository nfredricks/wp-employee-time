<?php
/*
Plugin Name: Employee Time Tracking
Plugin URI: http://URI_Of_Page_Describing_Plugin_and_Updates
Description: Allows tracking of specific users time on the site
Version: 1.0
Author: Nicholas Fredricks
Author URI: http://logikgate.com
License: GPL2

Copyright 2012  Nicholas Fredricks  (email : nfredricks@gmail.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as 
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/


add_filter('the_posts', 'add_scripts_if_logged_in'); // the_posts gets triggered before wp_head
add_action( 'wp_ajax_employ_track', 'employ_track_calc' );
add_action( 'wp_ajax_ab_start_track', 'clock_in_clock_out' );
add_action( 'wp_ajax_timesheet_grab', 'timesheet_grab');


function add_scripts_if_logged_in($posts){
  if(is_user_logged_in()){
    add_tracker();
  }

//if shortcode wp_enqueue_style('employ-track-style', plugins_url('style.css', __FILE__));
  return $posts;
    
}

add_action( 'admin_menu', 'employee_times' );

function employee_times() {
  $is_admin = check_admin();
  if($is_admin['admin']){
    $page = add_menu_page('Employee Timesheet', 'Timesheets', 'manage_options', 'employ-manager', 'employ_settings');
    add_action('admin_print_styles-' . $page, 'employee_times_admin_styles');
  }
  $page = add_menu_page('Employee Clock', 'Clock In/Out', 'manage_options', 'employ-clock', 'clock_in_clock_out');
  add_action('admin_print_styles-' . $page, 'clock_in_admin_styles');
  add_tracker();
}
function employee_times_admin_styles() {
  wp_enqueue_script('employ-manager-js', plugins_url('admin/manage.js', __FILE__), 'jquery');
  common_admin_styles();
}
function clock_in_admin_styles(){
  common_admin_styles();
}
function common_admin_styles(){
  wp_enqueue_style('employ-track-style', plugins_url('style.css', __FILE__));
}
function add_tracker(){
  wp_enqueue_script('employ_track', plugins_url('tracker.js', __FILE__), array('jquery'));
  wp_localize_script( 'employ_track', 'track', array( 'ajaxurl' => admin_url('admin-ajax.php') ) );
}


function employ_settings() {
  global $wpdb;

  if ( !current_user_can( 'manage_options' ) )  {
    wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
  }

    $hidden_field_name_employ = 'employ_submit_hidden';
    $hidden_field_name_export = 'export_submit_hidden';
    $export_button_name = 'export-time';

    $opt_name = 'tracking_types';
    $opt_val = get_option( $opt_name );
    if( isset($_POST[ $hidden_field_name_employ ]) && $_POST[ $hidden_field_name_employ ] == 'Y' ) {
      $opt_val = $_POST[ $opt_name ];
      update_option( $opt_name, $opt_val );
      update_timesheet_file();
?>
<div class="updated"><p><strong><?php _e('settings saved.'); ?></strong></p></div>

<?php
    }

    // Now display the settings editing screen

    echo '<div class="wrap">';
    echo "<h2>Employee Timesheets</h2>";  
?>
<form name="employ-admin" id="employ-admin" method="post" action="">
<p><input type="hidden" name="<?php echo $hidden_field_name_employ; ?>" id="<?php echo $hidden_field_name_employ; ?>" value="Y">
User types to track (separated by a comma):
<input type="text" name="<?php echo $opt_name; ?>" id="<?php echo $opt_name; ?>" value = "<?php echo $opt_val; ?>">
</p>
<p class="submit">
<input type="submit" name="Submit" class="button-primary" value="Save Changes" >
</p>
</form>
<form name="export-timesheet" id="export-timesheet" method="post" action="">
<p><input type="hidden" name="<?php echo $hidden_field_name_export; ?>" id="<?php echo $hidden_field_name_export; ?>" value="Y">
<input type="button" name="<?php echo $export_button_name; ?>" id="<?php echo $export_button_name; ?>" value="Export all timesheets">
</p>

</form>

<h2>Choose an Employee to View</h2>
<select id="employee_selector">
  <option>Select Employee</option>
<?php
    $table_name = $wpdb->prefix . "usermeta"; 
    $result = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE meta_key = 'wp_capabilities' "));
    $types = explode(',', $opt_val);

    foreach($result as $user){
        if(is_trackable($types, $user)){
          $tutor = get_user_by('id',$user->user_id);
          echo '<option value="'.$user->user_id.'">'.$tutor->display_name.'</option>';
        }
    }
?>
</select>
<div id="timesheet_space">

</div>
</div>
<?php
  
}

function clock_in_clock_out(){
  global $wpdb;
  $table_name = $wpdb->prefix . "employee_time";
  $tracking = is_tracking($table_name, $wpdb);
  $id = get_current_user_id();


  if(isset($_POST['ab_clocker']) && $_POST['ab_clocker'] == '1'){
    if(!$tracking){
      start_tracking();
      echo "started";
      die();
    }
    else{
      end_tracking($wpdb);
      echo "stopped";
      die();
    }

  }
  else{
    if((isset($_POST['info']) && $_POST['info'] == "1") || $_GET['clocker']=='admin-bar-control' ) { 
      if(!$tracking){
        start_tracking();
        $button = "Clock Out!";
      }
      else{
        end_tracking($wpdb);
        $button = "Clock In!";
      }
    }
    else{
      if($tracking){
        $button = "Clock Out!";
      }
      else{
        $button = "Clock In!";
      }
    
    ?>
    <div class=wrap>
      <h2>Time Tracker</h2>
      <form method=post action=#>
        <p><input type=hidden name=info value=1><input type=submit value="<?php _e($button); ?>"></p>
      </form>
        <?php 
        do_shortcode("[show-timesheet]"); 
        do_shortcode("[show-timesheet offset='1']")
        ?>

    </div>
    <?php
    }
  }

}
function timesheet_grab(){
  $offset = '0';
  if(isset($_POST['offset'])){
    $offset = mysql_real_escape_string(trim(strip_tags($_POST['offset'])));
  }
  if(isset($_POST['id'])){
    $id = mysql_real_escape_string(trim(strip_tags($_POST['id'])));
    do_shortcode('[show-timesheet offset='.$offset.' id='.$id.']');
    $offset = "1";
    do_shortcode('[show-timesheet offset='.$offset.' id='.$id.']');
    $offset = "2";
    do_shortcode('[show-timesheet offset='.$offset.' id='.$id.']');
    $offset = "3";
    do_shortcode('[show-timesheet offset='.$offset.' id='.$id.']');
  }
  else{
    do_shortcode('[show-timesheet offset='.$offset.']');
    $offset = "1";
    do_shortcode('[show-timesheet offset='.$offset.']');
    $offset = "2";
    do_shortcode('[show-timesheet offset='.$offset.']');
    $offset = "3";
    do_shortcode('[show-timesheet offset='.$offset.']');

  }
  die();
}

function employ_track_calc() {
  $gofs = get_option( 'gmt_offset' ); // get WordPress offset in hours
  $tz = date_default_timezone_get(); // get current PHP timezone
  date_default_timezone_set('Etc/GMT'.(($gofs < 0)?'+':'').-$gofs); // set the PHP timezone to match WordPress
  global $wpdb;
  $table_name = $wpdb->prefix ."employee_time";
  $hysteresis = 60; //time in seconds
  $today = date('Y-m-d');
  $cur_time = date('H:i:s');
  $track_check = track_check();

  
  if($track_check['track']){
    
    
    if(is_tracking($table_name, $wpdb)){
      if(time_since_last_up($cur_time, $table_name, $today, $wpdb) < $hysteresis){
        $update = array('end' => $cur_time);
        $where = array('user' => $track_check['id'], 'day' => $today, 'online' => 1 );
        $wpdb->update($table_name, $update, $where);
      }
      else{
        remove_online($track_check['id'], $today, $table_name, $wpdb); //user left the website and is now logged off
      }
    }
  }
  else{
  }

  //update everyones online status
  $all = $wpdb->get_results("SELECT * FROM $table_name WHERE online = 1");

  foreach($all as $entry){
    $end = $entry->end;
    $date = $entry->day;
    $id = $entry->user;
    list($year, $month, $day) = explode("-", $date);
    list($hour, $min, $sec) = explode(":",$end);
    $end_utime = mktime($hour, $min, $sec, $month, $day, $year);

    list($year, $month, $day) = explode("-", $today);
    list($hour, $min, $sec) = explode(":",$cur_time);
    $cur_utime = mktime($hour, $min, $sec, $month, $day, $year);

    if($cur_utime-$end_utime > $hysteresis){ //do comparison of times and make sure that the day is right 
      remove_online($id, $date, $table_name, $wpdb);
    }
    else if($date != $today){ //if they pass the time check but not the date make a new entry for today and force online bit to true
      remove_online($id, $date, $table_name, $wpdb);
      $rows_affected = $wpdb->insert( $table_name, array( 'user' => $id, 'day' => $today, 'online' => 1, 'start' => $cur_time, 'end' => $cur_time) );
    }


  }

  $all = $wpdb->get_results("SELECT * FROM $table_name WHERE online = 1");
  $ret_array = array();
  foreach($all as $entry){
    $id = $entry->user;
    $user_info = get_userdata($id);
    $ret_array[] = "<li>".$user_info->display_name."</li>";
  }
  $json_ret_array = json_encode($ret_array);
  echo $json_ret_array;


  date_default_timezone_set($tz); // set the PHP timezone back the way it was

  die(); // this is required to return a proper result
}


function employ_time_install () {
   global $wpdb;
   $table_name = $wpdb->prefix . "employee_time"; 

   $sql = "CREATE TABLE $table_name (
      id mediumint(9) NOT NULL AUTO_INCREMENT,
      user tinytext NOT NULL,
      day DATE DEFAULT '00-00-0000' NOT NULL,
      online tinyint(1) NOT NULL,
      start time DEFAULT '00:00:00' NOT NULL,
      end time DEFAULT '00:00:00' NOT NULL,
      UNIQUE KEY id (id)
    );";

    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    dbDelta($sql);
}

function employ_time_install_data () {
  add_option('tracking_types');
  update_option('tracking_types','administrator');

}

// add links/menus to the admin bar
function clock_in_and_out_admin_bar_render() {
  global $wp_admin_bar;
  global $wpdb;
  $table_name = $wpdb->prefix . "employee_time";
  $track = track_check(); 
  if($track['track']){
    $img_loc = plugins_url('img/clock.png', __FILE__);




    if(is_tracking($table_name, $wpdb)){
      $wp_admin_bar->add_menu( array(
        'parent' => 'top-secondary', // use 'false' for a root menu, or pass the ID of the parent menu
        'id' => 'ab_clocker', // link ID, defaults to a sanitized title value
        'title' => __('<img src="'.$img_loc.'" class="ab-icon"><span class="ab-label" data-next="Clock In">Clock Out</span>'), // link title
        'href' => admin_url( 'admin.php?page=employ-clock&clocker=admin-bar-control'), // name of file
        'meta' => array('class' => 'ab-clocker')
      ));
    }
    else{
      $wp_admin_bar->add_menu( array(
        'parent' => 'top-secondary', // use 'false' for a root menu, or pass the ID of the parent menu
        'id' => 'ab_clocker', // link ID, defaults to a sanitized title value
        'title' => __('<img src="'.$img_loc.'" class="ab-icon"><span class="ab-label" data-next="Clock Out">Clock In</span>'), // link title
        'href' => admin_url( 'admin.php?page=employ-clock&clocker=admin-bar-control'), // name of file
        'meta' => array('class' => 'ab-clocker')
      ));
    }

  }




}




add_action( 'wp_before_admin_bar_render', 'clock_in_and_out_admin_bar_render' );




register_activation_hook(__FILE__,'employ_time_install');
register_activation_hook(__FILE__,'employ_time_install_data');

require_once('functions.php');
require_once('shortcodes.php');
require_once('widget.php');


?>
